const express = require('express')
const cors = require('cors')
const app = express()
const bodyParser = require('body-parser')
const jsonParser = bodyParser.json()
// const bcrypt = require('bcrypt')
// const saltRounds = 10
const jwt = require('jsonwebtoken')
// const token = jwt.sign({ foo: 'bar' }, 'shhhhh')
const secret = 'Fullstack-Login-2022'
// const cookieSession = require('cookie-session')
// const PORT = process.env.PORT || 5000

app.use(cors())
app.use(express.json())

const mysql = require('mysql2');
// const connection = mysql.createConnection({
//     host: 'sql6.freemysqlhosting.net',
//     user: 'sql6491809',
//     password: 'VcJsx9MxiS',
//     database: 'sql6491809'
//   });
const connection = mysql.createConnection({
    host: 'g84t6zfpijzwx08q.cbetxkdyhwsb.us-east-1.rds.amazonaws.com',
    user: 'lldx4h9wmhxfmfhp',
    password: 'z93nqtskt8fq7pd3',
    database: 'an129rteiptdcifu'
  });

app.get('/', jsonParser, function (req, res, next) {
    res.json({status: 'ok', message: 'welcome'})
})

// app.post('/register', jsonParser, function (req, res, next) {
//     bcrypt.hash(req.body.pass, saltRounds, function(err, hash) {
//         connection.execute(
//             'INSERT INTO users (email, pass, fname, lname) VALUES (?, ?, ?, ?)',
//             [req.body.email, hash, req.body.fname, req.body.lname],
//             function(err, results, fields) {
//                 if (err) {
//                     res.json({status: 'error', message: err})
//                     //return
//                 }
//                 res.json({status: 'ok'})
//             }
//         );
//     });
// })

app.post('/register', jsonParser, function (req, res, next) {
    connection.execute(
        'INSERT INTO users (email, pass, fname, lname) VALUES (?, ?, ?, ?)',
        [req.body.email, req.body.pass, req.body.fname, req.body.lname],
        function(err, results, fields) {
            if (err) {
                res.json({status: 'error', message: err})
                //return
            }
            res.json({status: 'ok'})
        }
    );
})

app.post('/login', jsonParser, function (req, res, next) {
    connection.execute(
        `SELECT * FROM users WHERE email=?`,
        [req.body.email],
        function(err, users, fields) {
            try {
                if (req.body.pass === users[0].pass) {
                    var token = jwt.sign({ email: users[0].email }, secret, { expiresIn: '1h' })
                    res.json({status: 'ok', message: 'login success', token, activeID: users[0].id, show: req.body.pass+" "+users[0].pass})
                } else if (users.length == 0) {
                    res.json({status: 'error', message: 'no user found'})
                    //return
                }
                else {
                    res.json({status: 'error', message: 'login failed'})
                }
            } catch (err) {
                res.json({status: 'error', message: err.message})
            }
            
            // if (err) {
            //     res.json({status: 'error', message: err})
            //     //return
            // }
            // if (users.length == 0) {
            //     res.json({status: 'error', message: 'no user found'})
            //     //return
            // }
            // if (req.body.pass === users[0].pass) {
            //     var token = jwt.sign({ email: users[0].email }, secret, { expiresIn: '1h' })
            //     res.json({status: 'ok', message: 'login success', token, activeID: users[0].id, show: req.body.pass+" "+users[0].pass})
            // } else {
            //     res.json({status: 'error', message: 'login failed'})
            // }
            // bcrypt.compare(req.body.pass, users[0].pass, function(err, isLogin) {
            //     if (isLogin) {
            //         var token = jwt.sign({ email: users[0].email }, secret, { expiresIn: '1h' })
            //         res.json({status: 'ok', message: 'login success', token, activeID: users[0].id})
            //     } else {
            //         res.json({status: 'error', message: 'login failed'})
            //     }
            // });
        }
    );
})

app.post('/authen', jsonParser, function (req, res, next) {
    try {
        const token = req.headers.authorization.split(' ')[1]
        var decoded = jwt.verify(token, secret)
        res.json({status: 'ok', decoded})
    } catch (err) {
        res.json({status: 'error', message: err.message})
    }
})

app.get('/ashow/:id', jsonParser, function (req, res, next) {
    connection.execute(
        'SELECT * FROM grams WHERE id=?',
        [req.params.id],
        function(err, results, fields) {
            res.json({status: 'ok', info:results[0]});
            // res.json(results);
        }
    );
})

app.get('/showall', jsonParser, function (req, res, next) {
    connection.execute(
        'SELECT * FROM grams ORDER BY id DESC',
        function(err, results, fields) {
            res.json(results);
        }
    );
})

app.get('/user/:id', jsonParser, function (req, res, next) {
    connection.execute(
        `SELECT CONCAT(fname, ' ',lname) AS name FROM users WHERE id=?`,
        [req.params.id],
        function(err, results, fields) {
            res.json({status: 'ok',  info: results[0]});
        }
    );
})

app.get('/mygram/:id', jsonParser, function (req, res, next) {
    connection.execute(
        `SELECT * FROM grams WHERE user=? ORDER BY id DESC`,
        [req.params.id],
        function(err, results, fields) {
            res.json(results);
        }
    );
})

app.delete('/mygram/del', jsonParser, function (req, res, next) {
    connection.execute(
        `DELETE FROM grams WHERE id=?`,
        [req.body.id],
        function(err, results, fields) {
            if (err) {
                res.json({status: 'error', message: err})
            }
            if (results.affectedRows === 0) {
                res.json({status: 'error', message: 'no gram found'})
            } else {
                res.json({status: 'ok'})
                // res.json({results})
            }
        }
    );
})

app.post('/add', jsonParser, function (req, res, next) {
    connection.execute(
        'INSERT INTO grams (user, caption, image) VALUES (?, ?, ?)',
        [req.body.user, req.body.caption, req.body.image],
        function(err, results, fields) {
            if (err) {
                res.json({status: 'error', message: err})
                //return
            }
            res.json({status: 'ok'})
        }
    );
    //"createdAt": "CURRENT_TIMESTAMP",
    //"updatedAt": "0000-00-00 00:00:00.000000"
})

app.put('/edit', jsonParser, function (req, res, next) {
    connection.execute(
      'UPDATE grams SET caption=?, image=?, createdAt=?, updatedAt=CURRENT_TIMESTAMP WHERE id=?',
      [req.body.caption, req.body.image, req.body.createdAt, req.body.id],
      function(err, results) {
        if (err) {
            res.json({status: 'error', message: err})
        }
        if (results.affectedRows === 0) {
            res.json({status: 'error', message: 'no gram found'})
        } else {
            res.json({status: 'ok'})
            // res.json({results})
        }
      }
    );
})

// app.listen(PORT, jsonParser, function () {
//   console.log('CORS-enabled web server listening on port '+PORT)
// })

app.listen(process.env.PORT || 3000, function(){
    console.log("Express server listening on port %d in %s mode", this.address().port, app.settings.env);
})